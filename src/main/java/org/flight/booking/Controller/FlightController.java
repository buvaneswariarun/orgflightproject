package org.flight.booking.Controller;

import org.flight.booking.model.AjaxResponseBody;
import org.flight.booking.model.Flight;
import org.flight.booking.model.FlightBookingForm;
import org.flight.booking.model.FlightResultSetModel;
import org.flight.booking.repository.FlightRepository;
import org.flight.booking.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class FlightController {

    FlightRepository repository = new FlightRepository ();
    FlightService service = new FlightService ( repository );

    @RequestMapping("/search")
    public ArrayList <Flight> getAllFlights () {
        return (service.searchFlight ());
    }

    @RequestMapping("/flight/{flightNumber}")
    public Flight getFlightByNumber ( @PathVariable String flightNumber ) {
        return (service.getFlightByFlightNumber ( flightNumber ));
    }

    @RequestMapping("/flight")
    public ArrayList <Flight> searchFlightBasedOnSourceAndDestination ( @RequestParam String source
            , @RequestParam String destination , @RequestParam int noOfPassenger ) {
        return service.searchFlightBasedOnSourceAndDestination ( source , destination , noOfPassenger );
    }

    @PostMapping("/api/searchWithDate")
    public ResponseEntity <?> getSearchResultViaAjax ( @Valid @RequestBody FlightBookingForm flightBookingForm , Errors errors ) {

        AjaxResponseBody result = new AjaxResponseBody ();
        //If error, just return a 400 bad request, along with the error message
        if (errors.hasErrors ()) {
            result.setMsg ( errors.getAllErrors ()
                    .stream (). <String>map ( x -> x.getDefaultMessage () )
                    .collect ( Collectors.joining ( "," ) ) );
            return ResponseEntity.badRequest ().body ( result );
        }
        List <FlightResultSetModel> flights = service.searchFlightBasedOnSourceAndDestinationWithDate ( flightBookingForm );
        if (flights.isEmpty ()) {
            result.setMsg ( "no flights found!" );
        } else {
            result.setMsg ( "success" );
        }
        result.setResult ( flights );

        return ResponseEntity.ok ( result );

    }


}