package org.flight.booking.Controller;

import org.flight.booking.model.FlightClassNamesEnum;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
public class IndexController {

   //private final Logger logger = LoggerFactory.getLogger(IndexController.class);

    @GetMapping("/")
    public String index() {
        return "FlightSelection";
    }

    List <FlightClassNamesEnum> stateList = new ArrayList <FlightClassNamesEnum> ( Arrays.asList( FlightClassNamesEnum.values() ));


}