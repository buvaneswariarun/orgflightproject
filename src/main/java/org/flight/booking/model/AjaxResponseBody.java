package org.flight.booking.model;

import java.util.List;

public class AjaxResponseBody {

    private String msg;
    private List <FlightResultSetModel> result;



    public List <FlightResultSetModel> getResult () {
        return result;
    }

    public void setResult ( List <FlightResultSetModel> result ) {
        this.result = result;
    }

    public String getMsg () {
        return msg;
    }

    public void setMsg ( String msg ) {
        this.msg = msg;
    }

}