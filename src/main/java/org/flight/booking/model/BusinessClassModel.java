package org.flight.booking.model;

public class BusinessClassModel implements FlightClassModel {
    String flightNumber;
    int allottedSeats;
    int availableSeats;
    PriceModel businessClassPriceModel_instance;

    public BusinessClassModel ( String flightNumber , int allottedSeats , int availableSeats , double basePrice ) {
        this.flightNumber = flightNumber;
        this.allottedSeats = allottedSeats;
        this.availableSeats = availableSeats;
        businessClassPriceModel_instance = new BusinessPriceModel ( 13000.00 );
    }


    @Override
    public int getAllottedSeats () {
        return allottedSeats;
    }

    public void setAllottedSeats ( int allottedSeats ) {
        this.allottedSeats = allottedSeats;
    }
    @Override
    public int getAvailableSeats () {
        return availableSeats;
    }

    public void setAvailableSeats ( int availableSeats ) {
        this.availableSeats = availableSeats;
    }
    @Override
    public String className () {
        return "BUSINESS CLASS";
    }

}
