package org.flight.booking.model;

public class BusinessPriceModel implements PriceModel {

    double basePrice;
    double costPrice;

    public BusinessPriceModel ( double basePrice ) {
        this.basePrice = basePrice;
    }

    public double getBasePrice () {
        return basePrice;
    }

    public void setBasePrice ( double basePrice ) {
        this.basePrice = basePrice;
    }

    public double getCostPrice () {
        return costPrice;
    }

    public void setCostPrice ( double costPrice ) {
        this.costPrice = costPrice;
    }

    @Override
    public double calculateCostPrice ( double basePrice,int noOfTravellers ) {
        return 0;
    }
}

