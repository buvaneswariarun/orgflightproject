package org.flight.booking.model;

public class EconomyClassModel implements FlightClassModel {

    String flightNumber;
    int allottedSeats;
    int availableSeats;
    PriceModel economyPriceModel_instance;

    @Override
    public String className () {
        return "ECOMOMY CLASS";
    }

    public EconomyClassModel ( String flightNumber , int allottedSeats , int availableSeats , double basePrice ) {
        this.flightNumber = flightNumber;
        this.allottedSeats = allottedSeats;
        this.availableSeats = availableSeats;
        economyPriceModel_instance = new EconomyPriceModel (6000.00);

    }

    public int getAllottedSeats () {
        return allottedSeats;
    }

    public void setAllottedSeats ( int allottedSeats ) {
        this.allottedSeats = allottedSeats;
    }

    public int getAvailableSeats () {
        return availableSeats;
    }

    public void setAvailableSeats ( int availableSeats ) {
        this.availableSeats = availableSeats;
    }

}
