package org.flight.booking.model;

public class FirstClassModel implements FlightClassModel {
    String flightNumber;
    int allottedSeats;
    int availableSeats;
    PriceModel firstClassPriceModel_instance;

    public FirstClassModel ( String flightNumber , int allottedSeats , int availableSeats , double basePrice ) {
        this.flightNumber = flightNumber;
        this.allottedSeats = allottedSeats;
        this.availableSeats = availableSeats;
        firstClassPriceModel_instance = new FirstClassPriceModel ( 20000.00 );
    }


    @Override
    public String className () {
        return "FIRST CLASS";
    }

    public int getAllottedSeats () {
        return allottedSeats;
    }

    public void setAllottedSeats ( int allottedSeats ) {
        this.allottedSeats = allottedSeats;
    }

    public int getAvailableSeats () {
        return availableSeats;
    }

    public void setAvailableSeats ( int availableSeats ) {
        this.availableSeats = availableSeats;
    }

}
