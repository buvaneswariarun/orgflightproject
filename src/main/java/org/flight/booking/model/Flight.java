package org.flight.booking.model;

import java.util.Date;
import java.util.Map;

public class Flight {

    private String flightNumber;
    private int availbleSeats;
    private String sourceName;
    private String destinationName;
    private Date flightDepartureDate;

    private Map<String,FlightClassModel> seatingAvailabilityMap;

    public FlightModel getFlightModel_instance () {
        return flightModel_instance;
    }

    public void setFlightModel_instance ( FlightModel flightModel_instance ) {
        this.flightModel_instance = flightModel_instance;
    }

    public FlightClassModel getFlightClassModel_businessClass_instance () {
        return flightClassModel_businessClass_instance;
    }

    public void setFlightClassModel_businessClass_instance ( FlightClassModel flightClassModel_businessClass_instance ) {
        this.flightClassModel_businessClass_instance = flightClassModel_businessClass_instance;
    }

    public FlightClassModel getFlightClassModel_ecomonyClass_instance () {
        return flightClassModel_ecomonyClass_instance;
    }

    public void setFlightClassModel_ecomonyClass_instance ( FlightClassModel flightClassModel_ecomonyClass_instance ) {
        this.flightClassModel_ecomonyClass_instance = flightClassModel_ecomonyClass_instance;
    }

    public FlightClassModel getFlightClassModel_firstClass_instance () {
        return flightClassModel_firstClass_instance;
    }

    public Map getSeatingAvailabilityMap () {
        return seatingAvailabilityMap;
    }

    public void setSeatingAvailabilityMap ( Map seatingAvailabilityMap ) {
        this.seatingAvailabilityMap = seatingAvailabilityMap;
    }

    public void setFlightClassModel_firstClass_instance ( FlightClassModel flightClassModel_firstClass_instance ) {
        this.flightClassModel_firstClass_instance = flightClassModel_firstClass_instance;
    }

    private FlightModel flightModel_instance;
    private FlightClassModel flightClassModel_businessClass_instance;
    private FlightClassModel flightClassModel_ecomonyClass_instance;
    private FlightClassModel flightClassModel_firstClass_instance;

    public Flight ( String flightNumber , int availbleSeats , String sourceName , String destinationName ,
                    Date flightDepartureDate ,
                    FlightModel flightModel_instance ,
                    Map seatingAvailabilityMap
                    ) {
        this.flightNumber = flightNumber;
        this.availbleSeats = availbleSeats;
        this.sourceName = sourceName;
        this.destinationName = destinationName;
        this.flightDepartureDate = flightDepartureDate;
        this.seatingAvailabilityMap = seatingAvailabilityMap;
        this.flightModel_instance = flightModel_instance;
    }

    public Flight ( String flightNumber , int availbleSeats , String sourceName , String destinationName ,
                    Date flightDepartureDate , FlightModel flightModel_instance ,
                    FlightClassModel flightClassModel_businessClass_instance ,
                    FlightClassModel flightClassModel_ecomonyClass_instance ,
                    FlightClassModel flightClassModel_firstClass_instance ) {
        this.flightNumber = flightNumber;
        this.availbleSeats = availbleSeats;
        this.sourceName = sourceName;
        this.destinationName = destinationName;
        this.flightDepartureDate = flightDepartureDate;
        this.flightModel_instance = flightModel_instance;
        this.flightClassModel_businessClass_instance = flightClassModel_businessClass_instance;
        this.flightClassModel_ecomonyClass_instance = flightClassModel_ecomonyClass_instance;
        this.flightClassModel_firstClass_instance = flightClassModel_firstClass_instance;
    }




//    public Flight ( String flightNumber , String flightModel , int availbleSeats , String sourceName , String destinationName , Date travelDate) {
//        this.flightNumber = flightNumber;
//        this.availbleSeats = availbleSeats;
//        this.sourceName = sourceName;
//        this.destinationName = destinationName;
//        this.dateOfTravel = travelDate;
//        flightClassModel_businessClass_instance = new BusinessClassModel ();
//        flightClassModel_ecomonyClass_instance = new EconomyClassModel ();
//        flightClassModel_firstClass_instance = new FirstClassModel ();
//    }

    public Flight ( String flightNumber , String flightModel , int availbleSeats ) {
        this.flightNumber = flightNumber;
        this.availbleSeats = availbleSeats;
    }

    public Flight () {
    }




    public Date getFlightDepartureDate () {
        return flightDepartureDate;
    }

    public void setFlightDepartureDate ( Date flightDepartureDate ) {
        this.flightDepartureDate = flightDepartureDate;
    }

    public String getSourceName () {
        return sourceName;
    }

    public void setSourceName ( String sourceName ) {
        this.sourceName = sourceName;
    }

    public String getDestinationName () {
        return destinationName;
    }

    public void setDestinationName ( String destinationName ) {
        this.destinationName = destinationName;
    }

    public String getFlightNumber () {
        return flightNumber;
    }

    public void setFlightNumber ( String flightNumber ) {
        this.flightNumber = flightNumber;
    }



    public int getAvailbleSeats () {
        return availbleSeats;
    }

    public void setAvailbleSeats ( int availbleSeats ) {
        this.availbleSeats = availbleSeats;
    }

//    @Override
//    public String toString() {
//        return "Flight{" +
//                "Model='" + flightModel + '\'' +
//                ", Number ='" + flightNumber + '\'' +
//                ", Source   ='" + sourceName + '\'' +
//                ", Destination   ='" + destinationName + '\'' +
//                '}';
//    }

}
