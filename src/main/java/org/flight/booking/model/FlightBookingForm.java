package org.flight.booking.model;

import javax.validation.constraints.NotEmpty;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FlightBookingForm {

    @NotEmpty(message = "Source name can't be empty!")
    private String sourceName;

    @NotEmpty(message = "Destination can't be empty!")
    private String destinationName;

    //@(message = "Number of Passengers travelling cannot be empty")
    private int numberOfPassengers;

    private Date dateOfTravel;

    private String travelClass;

    public String getTravelClass () {
        return travelClass;
    }

    public void setTravelClass ( String travelClass ) {
        this.travelClass = travelClass;
    }

    public Date getDateOfTravel () {

        return dateOfTravel;
    }

    public void setDateOfTravel ( Date dateOfTravel )  {


//       DateFormat sdf = new SimpleDateFormat ("EEE MMM d HH:mm:ss zzz yyyy");
//       String formatted = sdf.format (dateOfTravel);
//        Date tempDate = sdf.parse(formatted);
        this.dateOfTravel = dateOfTravel;

    }

    public int getNumberOfPassengers () {
        return numberOfPassengers;
    }

    public void setNumberOfPassengers ( int numberOfPassengers ) {
        this.numberOfPassengers = numberOfPassengers;
    }


    public String getSourceName () {
        return sourceName;
    }

    public void setSourceName ( String sourceName ) {
        this.sourceName = sourceName;
    }

    public void setDestinationName ( String destinationName ) {
        this.destinationName = destinationName;
    }

    public String getDestinationName () {
        return destinationName;
    }

    public static Date makeDate(Date inputDate){
        String pattern = "dd-MM-yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String newDate = simpleDateFormat.format(new Date());
        Date result;
        try {
            result = simpleDateFormat.parse (newDate  );
            return result;
        } catch (ParseException e) {
            e.printStackTrace ();
        }
        return null;
    }

}

