package org.flight.booking.model;

public interface FlightClassModel {
    public String className();
    public int getAvailableSeats();
    public int getAllottedSeats();

}
