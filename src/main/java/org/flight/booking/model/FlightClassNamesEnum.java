package org.flight.booking.model;

public enum FlightClassNamesEnum
{
    ECONOMY_CLASS("EC"),
    BUSINESS_CLASS("BC"),
    FIRST_CLASS("FC");

    private String fullClassNames;

    private FlightClassNamesEnum ( String s )
    {
        fullClassNames = s;
    }

    public String getFullClassNames()
    {
        return fullClassNames;
    }
    }
