package org.flight.booking.model;

public class FlightModel {
    private String flightNumber;
    private String flightModelType;
    private int totalNumberOfSeats;
    private int totalEconomySeatsProvided;
    private int availableEconomySeatsForBooking;
    private int totalFirstClassSeatsProvided;
    private int availableFirstClassSeatsForBooking;
    private int totalBusinessClassSeatsProvided;
    private int availableBusinessClassSeatsForBooking;

    public FlightModel ( String flightNumber ,
                         String flightModelType ,
                         int totalNumberOfSeats ,
                         int totalEconomySeatsProvided ,
                         int availableEconomySeatsForBooking ,
                         int totalFirstClassSeatsProvided ,
                         int availableFirstClassSeatsForBooking ,
                         int totalBusinessClassSeatsProvided ,
                         int availableBusinessClassSeatsForBooking ) {
        this.flightNumber = flightNumber;
        this.flightModelType = flightModelType;
        this.totalNumberOfSeats = totalNumberOfSeats;
        this.totalEconomySeatsProvided = totalEconomySeatsProvided;
        this.availableEconomySeatsForBooking = availableEconomySeatsForBooking;
        this.totalFirstClassSeatsProvided = totalFirstClassSeatsProvided;
        this.availableFirstClassSeatsForBooking = availableFirstClassSeatsForBooking;
        this.totalBusinessClassSeatsProvided = totalBusinessClassSeatsProvided;
        this.availableBusinessClassSeatsForBooking = availableBusinessClassSeatsForBooking;
    }


    public String getFlightNumber () {
        return flightNumber;
    }

    public void setFlightNumber ( String flightNumber ) {
        this.flightNumber = flightNumber;
    }

    public String getFlightModelType () {
        return flightModelType;
    }

    public void setFlightModelType ( String flightModelType ) {
        this.flightModelType = flightModelType;
    }

    public int getTotalNumberOfSeats () {
        return totalNumberOfSeats;
    }

    public void setTotalNumberOfSeats ( int totalNumberOfSeats ) {
        this.totalNumberOfSeats = totalNumberOfSeats;
    }

    public int getTotalEconomySeatsProvided () {
        return totalEconomySeatsProvided;
    }

    public void setTotalEconomySeatsProvided ( int totalEconomySeatsProvided ) {
        this.totalEconomySeatsProvided = totalEconomySeatsProvided;
    }

    public int getAvailableEconomySeatsForBooking () {
        return availableEconomySeatsForBooking;
    }

    public void setAvailableEconomySeatsForBooking ( int availableEconomySeatsForBooking ) {
        this.availableEconomySeatsForBooking = availableEconomySeatsForBooking;
    }

    public int getTotalFirstClassSeatsProvided () {
        return totalFirstClassSeatsProvided;
    }

    public void setTotalFirstClassSeatsProvided ( int totalFirstClassSeatsProvided ) {
        this.totalFirstClassSeatsProvided = totalFirstClassSeatsProvided;
    }

    public int getAvailableFirstClassSeatsForBooking () {
        return availableFirstClassSeatsForBooking;
    }

    public void setAvailableFirstClassSeatsForBooking ( int availableFirstClassSeatsForBooking ) {
        this.availableFirstClassSeatsForBooking = availableFirstClassSeatsForBooking;
    }

    public int getTotalBusinessClassSeatsProvided () {
        return totalBusinessClassSeatsProvided;
    }

    public void setTotalBusinessClassSeatsProvided ( int totalBusinessClassSeatsProvided ) {
        this.totalBusinessClassSeatsProvided = totalBusinessClassSeatsProvided;
    }

    public int getAvailableBusinessClassSeatsForBooking () {
        return availableBusinessClassSeatsForBooking;
    }

    public void setAvailableBusinessClassSeatsForBooking ( int availableBusinessClassSeatsForBooking ) {
        this.availableBusinessClassSeatsForBooking = availableBusinessClassSeatsForBooking;
    }


}
