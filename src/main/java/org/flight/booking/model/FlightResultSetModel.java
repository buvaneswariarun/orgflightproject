package org.flight.booking.model;

import java.util.Date;

public class FlightResultSetModel {
    private String flightNumber;
    private String sourceName;
    private String destinationName;
    private Date flightDepartureDate;
    private String travelClass;
    private int availbleSeatsInClassSelected;
    private int numberOfPassengerTravelling;

    public FlightResultSetModel () {
    }

    public FlightResultSetModel ( String flightNumber , int availbleSeatsInClassSelected , String sourceName ,
                                  String destinationName ,
                                  Date flightDepartureDate ,
                                  String travelClass ,
                                  int numberOfPassengerTravelling ) {
        this.flightNumber = flightNumber;
        this.availbleSeatsInClassSelected = availbleSeatsInClassSelected;
        this.sourceName = sourceName;
        this.destinationName = destinationName;
        this.flightDepartureDate = flightDepartureDate;
        this.travelClass = travelClass;
        this.numberOfPassengerTravelling = numberOfPassengerTravelling;
    }

    public String getFlightNumber () {
        return flightNumber;
    }

    public void setFlightNumber ( String flightNumber ) {
        this.flightNumber = flightNumber;
    }

    public int getAvailbleSeatsInClassSelected () {
        return availbleSeatsInClassSelected;
    }

    public void setAvailbleSeatsInClassSelected ( int availbleSeatsInClassSelected ) {
        this.availbleSeatsInClassSelected = availbleSeatsInClassSelected;
    }

    public String getSourceName () {
        return sourceName;
    }

    public void setSourceName ( String sourceName ) {
        this.sourceName = sourceName;
    }

    public String getDestinationName () {
        return destinationName;
    }

    public void setDestinationName ( String destinationName ) {
        this.destinationName = destinationName;
    }

    public Date getFlightDepartureDate () {
        return flightDepartureDate;
    }

    public void setFlightDepartureDate ( Date flightDepartureDate ) {
        this.flightDepartureDate = flightDepartureDate;
    }

    public String getTravelClass () {
        return travelClass;
    }

    public void setTravelClass ( String travelClass ) {
        this.travelClass = travelClass;
    }

    public int getNumberOfPassengerTravelling () {
        return numberOfPassengerTravelling;
    }

    public void setNumberOfPassengerTravelling ( int numberOfPassengerTravelling ) {
        this.numberOfPassengerTravelling = numberOfPassengerTravelling;
    }
}
