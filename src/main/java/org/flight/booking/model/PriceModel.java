package org.flight.booking.model;

public interface PriceModel {
    public double calculateCostPrice(double basePrice,int noOfTravellers);
}
