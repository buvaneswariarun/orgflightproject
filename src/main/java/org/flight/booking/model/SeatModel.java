package org.flight.booking.model;

public class SeatModel {

    String seatNumber;
    String seatClass;

    public SeatModel ( String seatNumber , String seatClass ) {
        this.seatNumber = seatNumber;
        this.seatClass = seatClass;
    }

    public String getSeatNumber () {
        return seatNumber;
    }

    public void setSeatNumber ( String seatNumber ) {
        this.seatNumber = seatNumber;
    }

    public String getSeatClass () {
        return seatClass;
    }

    public void setSeatClass ( String seatClass ) {
        this.seatClass = seatClass;
    }


}
