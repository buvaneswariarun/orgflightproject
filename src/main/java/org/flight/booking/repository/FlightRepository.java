package org.flight.booking.repository;

import org.flight.booking.model.*;
import org.springframework.stereotype.Repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Repository
public class FlightRepository {

    private Map <String, Flight> repository;

    public FlightRepository () {
        this.repository = new HashMap <String, Flight> ();

    }



    public void loadFLights ()  {


        FlightModel flightMod_AIRBUS_319 = new FlightModel ( "CARR_NAME_001","AIRBUS_319", 144,
                144,144,
                5,5,
                10,10 );
        FlightModel flightMod_BOEING = new FlightModel ( "CARR_NAME_002","BOEING_777", 238,
                195,195,
                8,8,
                35,35 );
        FlightModel flightMod_A321 = new FlightModel ( "CARR_NAME_003","AIRBUS_321", 172,
                152,152,
                0,0,
                20,20 );

        FlightClassModel economyClassModel_instance_AIRBUS_319 = new EconomyClassModel ("CARR_NAME_001",144,144,0.0);
        FlightClassModel businessClassModel_instance_AIRBUS_319 = new BusinessClassModel ("CARR_NAME_001",5,5,0.0);
        FlightClassModel firstClassModel_instance_AIRBUS_319 = new FirstClassModel ("CARR_NAME_001",10,10,0.0);

        FlightClassModel economyClassModel_instance_BOEING = new EconomyClassModel ("CARR_NAME_002",195,195,0.0);
        FlightClassModel businessClassModel_instance_BOEING = new BusinessClassModel ("CARR_NAME_002",35,35,0.0);
        FlightClassModel firstClassModel_instance_BOEING = new FirstClassModel ("CARR_NAME_002",8,8,0.0);

        FlightClassModel economyClassModel_instance_AIRBUS_321 = new EconomyClassModel ("CARR_NAME_003",152,152,0.0);
        FlightClassModel businessClassModel_instance_AIRBUS_321 = new BusinessClassModel ("CARR_NAME_003",0,0,0.0);
        FlightClassModel firstClassModel_instance_AIRBUS_321 = new FirstClassModel ("CARR_NAME_003",20,20,0.0);


        Map<String, FlightClassModel> seatingArrangement_AIRBUS_319 = new HashMap <String,FlightClassModel> (  );
        Map<String, FlightClassModel> seatingArrangement_BOEING = new HashMap <String,FlightClassModel> (  );
        Map<String, FlightClassModel> seatingArrangement_AIRBUS_321 = new HashMap <String,FlightClassModel> (  );

        seatingArrangement_AIRBUS_319.put("BUSINESS",businessClassModel_instance_AIRBUS_319);
        seatingArrangement_AIRBUS_319.put("ECONOMY",economyClassModel_instance_AIRBUS_319);
        seatingArrangement_AIRBUS_319.put("FIRST",firstClassModel_instance_AIRBUS_319);
        seatingArrangement_BOEING.put("BUSINESS",businessClassModel_instance_BOEING);
        seatingArrangement_BOEING.put("ECONOMY",economyClassModel_instance_BOEING);
        seatingArrangement_BOEING.put("FIRST",firstClassModel_instance_BOEING);
        seatingArrangement_AIRBUS_321.put("BUSINESS",businessClassModel_instance_AIRBUS_321);
        seatingArrangement_AIRBUS_321.put("ECONOMY",economyClassModel_instance_AIRBUS_321);
        seatingArrangement_AIRBUS_321.put("FIRST",firstClassModel_instance_AIRBUS_321);



        repository.put ( "CARR_001" , new Flight("CARR_001",144,
                "DELHI","HYDERABAD",makeDate (),
                flightMod_AIRBUS_319,
                seatingArrangement_AIRBUS_319) );
        repository.put ( "CARR_002", new Flight("CARR_002",238,
                "DELHI","HYDERABAD",makeDate (),
                flightMod_BOEING,
                seatingArrangement_BOEING) );
        repository.put ( "CARR_003", new Flight("CARR_003",152,
                "DELHI","HYDERABAD",makeDate (),
                flightMod_A321,
                seatingArrangement_AIRBUS_321) );

        repository.put ( "CARR_004" , new Flight("CARR_004",144,
                "MUMBAI","CHENNAI",makeDate (),
                flightMod_AIRBUS_319,
                seatingArrangement_AIRBUS_319) );
        repository.put ( "CARR_005", new Flight("CARR_005",238,
                "GOA","KOCHIN",makeDate (),
                flightMod_BOEING,
                seatingArrangement_BOEING) );
        repository.put ( "CARR_006", new Flight("CARR_006",152,
                "DELHI","HYDERBAD",makeDate (),
                flightMod_A321,
                seatingArrangement_AIRBUS_321) );
        repository.put ( "CARR_007" , new Flight("CARR_007",144,
                "DELHI","HYDERABAD",makeDate (),
                flightMod_AIRBUS_319,
                seatingArrangement_AIRBUS_319) );
        repository.put ( "CARR_008", new Flight("CARR_008",238,
                "MUMBAI","HYDERABAD",makeDate (),
                flightMod_BOEING,
                seatingArrangement_BOEING) );
        repository.put ( "CARR_009", new Flight("CARR_009",152,
                "DELHI","CHENNAI",makeDate (),
                flightMod_A321,
                seatingArrangement_AIRBUS_321) );
        repository.put ( "CARR_010", new Flight("CARR_010",238,
                "DELHI","CHENNAI",makeDate (),
                flightMod_BOEING,
                seatingArrangement_BOEING) );
    }

    ArrayList <Flight> listOfFlights () {
        loadFLights ();
        return new ArrayList <Flight> ( repository.values () );
    }


    public ArrayList <Flight> getFlights () {
        loadFLights ();
        return listOfFlights ();
    }

//    public static void main ( String[] args ) {
//        FlightRepository rep = new FlightRepository ();
//        rep.getFlights ();
//    }

    public Flight retrieve ( String flightNumber ) {
        loadFLights ();
        return repository.get ( flightNumber );
    }


    public Flight search ( String source , String destination , int numberOfPassenger ) {
        loadFLights ();
        Collection <Flight> flights = repository.values ();
        for (Flight flight : flights) {
            if ((flight.getSourceName ().equalsIgnoreCase ( source )) && (flight.getDestinationName ().equalsIgnoreCase ( destination ))) {
                if (flight.getAvailbleSeats () != 0) {
                    return flight;
                } else {
                    return null;
                }
            }
        }
        return null;
    }

    //
    public static Date makeDate(){
        String pattern = "dd-MM-yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String newDate = simpleDateFormat.format(new Date());
        Date result;
        try {
             result = simpleDateFormat.parse (newDate  );
             return result;
        } catch (ParseException e) {
            e.printStackTrace ();
        }
        return null;
    }
}