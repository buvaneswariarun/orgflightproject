package org.flight.booking.repository;

import org.flight.booking.model.Flight;
import org.flight.booking.model.SeatModel;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class SeatRepository {

    private Map <String, SeatModel> seatRepository;

    public SeatRepository () {
        this.seatRepository = new HashMap <String, SeatModel> ();

    }
    public void loadSeats(){
        seatRepository.put ( "1A",new SeatModel ("1A",""));
        seatRepository.put ( "1B",new SeatModel ("1B",""));
        seatRepository.put ( "1C",new SeatModel ("1C","" ));
        seatRepository.put ( "1D",new SeatModel ("1D","" ));
        seatRepository.put ( "1E",new SeatModel ("1E",""));
        seatRepository.put ( "2A",new SeatModel ("2A",""));
        seatRepository.put ( "2B",new SeatModel ("2B",""));
        seatRepository.put ( "2C",new SeatModel ("2C",""));
        seatRepository.put ( "2D",new SeatModel ("2D",""));
        seatRepository.put ( "2E",new SeatModel ("2E",""));
        seatRepository.put ( "3A",new SeatModel ("3A",""));
        seatRepository.put ( "3B",new SeatModel ("3B",""));
        seatRepository.put ( "3C",new SeatModel ("3C",""));
        seatRepository.put ( "3D",new SeatModel ("3D",""));
        seatRepository.put ( "3E",new SeatModel ("3E",""));
        seatRepository.put ( "4A",new SeatModel ("4A",""));
        seatRepository.put ( "4B",new SeatModel ("4B",""));
        seatRepository.put ( "4C",new SeatModel ("4C",""));
        seatRepository.put ( "4D",new SeatModel ("4D",""));
        seatRepository.put ( "4E",new SeatModel ("4E",""));
        seatRepository.put ( "5A",new SeatModel ("5A","" ));
        seatRepository.put ( "5B",new SeatModel ("5B",""));
        seatRepository.put ( "5C",new SeatModel ("5C",""));
        seatRepository.put ( "5D",new SeatModel ("5D",""));
        seatRepository.put ( "5E",new SeatModel ("5E",""));

















    }

}
