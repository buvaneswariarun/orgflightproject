package org.flight.booking.service;

import org.flight.booking.model.Flight;
import org.flight.booking.model.FlightBookingForm;
import org.flight.booking.model.FlightClassModel;
import org.flight.booking.model.FlightResultSetModel;
import org.flight.booking.repository.FlightRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

public class FlightService {

    private FlightRepository repository;

    @Autowired
    public FlightService ( FlightRepository repository ) {

        this.repository = repository;
        this.repository.loadFLights ();
    }

    public FlightService () {

    }

    public ArrayList <Flight> searchFlight () {

        return this.repository.getFlights ();
    }

    public Flight getFlightByFlightNumber ( String flightNumber ) {

        return this.repository.retrieve ( flightNumber );
    }

    public ArrayList <Flight> searchFlightBasedOnSourceAndDestination ( String source , String destination , int noOfPassenger ) {
        Collection <Flight> flights = this.repository.getFlights ();
        ArrayList <Flight> flightList = new ArrayList <Flight> ();
        if (noOfPassenger == 0) {
            noOfPassenger = 1;
        }
        for (Flight flight : flights) {
            System.out.println ( flight.getSourceName () + flight.getDestinationName () );
            if ((flight.getSourceName ().equalsIgnoreCase ( source )) &&
                    (flight.getDestinationName ().equalsIgnoreCase ( destination ))) {
                if (flight.getAvailbleSeats () > noOfPassenger) {
                    flightList.add ( flight );

                } else {
                    return flightList;
                }
            }
        }
        return flightList;
    }

    public ArrayList <FlightResultSetModel> searchFlightBasedOnSourceDestinationTravelClassPassengers (
            String source , String destination , int noOfPassenger , String travelClass ) {
        Collection <Flight> flights = this.repository.getFlights ();
        ArrayList <FlightResultSetModel> flightList = new ArrayList <FlightResultSetModel> ();
        if (noOfPassenger == 0) {
            noOfPassenger = 1;
        }
        for (Flight flight : flights) {
            System.out.println ( flight.getSourceName () + flight.getDestinationName () );
            if ((flight.getSourceName ().equalsIgnoreCase ( source )) &&
                    (flight.getDestinationName ().equalsIgnoreCase ( destination ))) {
                if (flight.getSeatingAvailabilityMap ().get ( travelClass ) != null) {
                    FlightClassModel travelClassModel = (FlightClassModel) flight.getSeatingAvailabilityMap ().get ( travelClass );
                    if (travelClassModel.getAvailableSeats () > 0) {
                        FlightResultSetModel resultFlight = new FlightResultSetModel ();
                        resultFlight.setFlightDepartureDate ( flight.getFlightDepartureDate () );
                        resultFlight.setSourceName ( flight.getSourceName () );
                        resultFlight.setDestinationName ( flight.getDestinationName () );
                        resultFlight.setFlightNumber ( flight.getFlightNumber () );
                        resultFlight.setTravelClass ( travelClass );
                        resultFlight.setNumberOfPassengerTravelling ( noOfPassenger );
                        resultFlight.setAvailbleSeatsInClassSelected ( ((FlightClassModel) flight.getSeatingAvailabilityMap ().get ( travelClass )) .getAvailableSeats ());
                        flightList.add ( resultFlight );
                    }
                }
            }
        }
        return flightList;
    }

    public ArrayList <FlightResultSetModel> searchFlightBasedOnSourceAndDestinationNew ( FlightBookingForm flightBookingForm ) {

        ArrayList <FlightResultSetModel> resultList = searchFlightBasedOnSourceDestinationTravelClassPassengers (
                flightBookingForm.getSourceName () ,
                flightBookingForm.getDestinationName () ,
                flightBookingForm.getNumberOfPassengers () ,
                flightBookingForm.getTravelClass () );
        return resultList;
    }

    public ArrayList <FlightResultSetModel> searchFlightBasedOnSourceAndDestinationWithDate ( FlightBookingForm flightBookingForm ) {
        Collection <Flight> flights = this.repository.getFlights ();
        ArrayList <FlightResultSetModel> result = new ArrayList <FlightResultSetModel> ();
        for (Flight flight : flights) {
            Date d1 = makeDate (flightBookingForm.getDateOfTravel ());
            Date d2 = makeDate ( flight.getFlightDepartureDate () );
            flight.setFlightDepartureDate ( d2 );
            if ((d1.compareTo(d2 )== 0)) {
                result = searchFlightBasedOnSourceAndDestinationNew ( flightBookingForm );
            }

        }
        return result;
    }
    public static Date makeDate(Date inDate){
        String pattern = "dd-MM-yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String newDate = simpleDateFormat.format(inDate);
        Date result;
        try {
            result = simpleDateFormat.parse (newDate  );
            return result;
        } catch (ParseException e) {
            e.printStackTrace ();
        }
        return null;
    }

    public ArrayList <String> getAllSources () {

        ArrayList <Flight> flights = repository.getFlights ();
        ArrayList <String> availableSources = new ArrayList <String> ();
        for (Flight flight : flights) {
            availableSources.add ( flight.getSourceName () );
        }
        return availableSources;
    }

    public ArrayList <String> getAllDestinations () {

        ArrayList <Flight> flights = repository.getFlights ();
        ArrayList <String> availableDestinations = new ArrayList <String> ();
        for (Flight flight : flights) {
            availableDestinations.add ( flight.getDestinationName () );
        }
        return availableDestinations;
    }

}


