package org.flight.booking.service;

import org.flight.booking.model.Flight;
import org.flight.booking.repository.FlightRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)

public class FlightServiceTest {
    //FlightRepository repository;

    @Mock
    FlightService mockFlightService;
    FlightRepository repository;

    @BeforeEach
    void setUp () {
        repository = mock(FlightRepository.class);
        mockFlightService = new FlightService ( repository );
        when ( mockFlightService.
                searchFlightBasedOnSourceAndDestination ( "DELHI" ,"HYDERABAD",2) )
                .thenReturn ( new ArrayList<Flight>());
    }

//    public void loadFLights(){
//        repository.put ( "BOE777-01" , new FlightModel ( "BOE777-01" , "BOEING777" , 100 , "DELHI" , "HYDERABAD" ) );
//        repository.put ( "A319-01" , new FlightModel ( "A319-01" , "Airbus A319 " , 100 , "DELHI" , "HYDERABAD" ) );
//        repository.put ( "A321-01" , new FlightModel ( "A321-01" , "Airbus A321" , 100 , "DELHI" , "HYDERABAD" ) );
//        repository.put ( "A321-02" , new FlightModel ( "A321-02" , "Airbus A321" , 100 , "MUMBAI" , "CHENNAI" ) );
//        repository.put ( "A321-03" , new FlightModel ( "A321-03" , "Airbus A321" , 100 , "CHENNAI" , "HYDERABAD" ) );
//
//    }

    @Test
    public void whenSourceAndDestinationNotGivenSearch () {
        ArrayList <Flight> flightList = mockFlightService.searchFlightBasedOnSourceAndDestination ( "" , "" , 0 );
        assertEquals(new ArrayList <> (  ), flightList );
    }

    @Test
    public void whenSourceAndDestinationNotGivenSearchWithPassengers () {
        ArrayList <Flight> flightList = mockFlightService.searchFlightBasedOnSourceAndDestination ( "" , "" , 1 );
        assertEquals ( new ArrayList <> (  ) , flightList );
    }

    @Test
    public void whenSourceAndDestinationGivenSearchWithNoPassengers () {

        ArrayList <Flight> flightList = mockFlightService.searchFlightBasedOnSourceAndDestination ( "MUMBAI" , "DELHI" , 0 );
        assertEquals ( flightList.size (),0   );
    }
    @Test
    public void whenSourceAndDestinationGivenSearchWithNoPassenger (){
        ArrayList <Flight> flightList = mockFlightService.searchFlightBasedOnSourceAndDestination ( "DELHI" , "HYDERABAD" , 0 );
        assertEquals ( flightList.size (),3   );
    }
    @Test
    public void whenSourceAndDestinationGivenSearchWithPassengerLessThanAvaialableSeats (){
        ArrayList <Flight> flightList = mockFlightService.searchFlightBasedOnSourceAndDestination ( "CHENNAI" , "HYDERABAD" , 10 );
        assertEquals ( flightList.size (),0  );
    }
}