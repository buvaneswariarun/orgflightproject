$(document).ready(function () {

    $("#flight-booking-form").submit(function (event) {

        //stop submit the form event. Do this manually using ajax post function
        event.preventDefault($( "#dateOfTravel" ).val());
        var flightBookingForm = {}
         var src = $("#sourceName").val();

        if(src.length ===0) {
                        alert('Please fill in Source');
                        return false;
                    }else{
                    flightBookingForm["sourceName"] = $("#sourceName").val();
                    }
       var dest = $("#destinationName").val();
        if(dest.length===0) {
                                alert('Please fill in Destination');
                                return false;
                        }else{
                            flightBookingForm["destinationName"] = $("#destinationName").val();
                        }
        var np = $("#numberOfPassengers").val();
        if((np===0)||(np.length===0)){
                 alert('No Passenger selected. Showing result for 1 passenger');
                 flightBookingForm["numberOfPassengers"] = $("#numberOfPassengers").val();
        }
        var date = new Date($( "#dateOfTravel" ).val());
        var jsonDate = date.toJSON();
        flightBookingForm["dateOfTravel"] =jsonDate;
        flightBookingForm["travelClass"]   =     $( "#travelClass option:selected" ).text();

        $("#btn-search").prop("disabled", true);

        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/api/searchWithDate",
            data: JSON.stringify(flightBookingForm),
            dataType: 'json',
            cache: false,
            timeout: 600000,
            success: function (data) {
                    var tr;
                    for (var i = 0; i < data.result.length; i++) {
                        tr = $("<tr/>");
                        tr.append("<td>" + data.result[i].flightNumber + "</td>");
                        tr.append("<td>" + data.result[i].sourceName + "</td>");
                        tr.append("<td>" + data.result[i].destinationName + "</td>");
                        tr.append("<td>" + data.result[i].flightDepartureDate + "</td>");
                        tr.append("<td>" + data.result[i].travelClass + "</td>");
                        tr.append("<td>" + data.result[i].availbleSeatsInClassSelected+ "</td>");

                        $("#result").find("tbody").append(tr);
                        alert(tr);
                    }
//                   var json = "<h4>Ajax Response</h4><pre>"
//                                        + JSON.stringify(data, null, 4) + "</pre>";
//                    document.getElementById("result").innerHTML = tr;
                                    $('#feedback').html(json);
                 console.log("SUCCESS : ", data);

                    },

            error: function (e) {

                var json = "<h4>Ajax Response Error</h4><pre>"
                    + e.responseText + "</pre>";
                $('#feedback').html(json);

                console.log("ERROR : ", e);
                $("#btn-search").prop("disabled", false);

            }
        });

    });

});